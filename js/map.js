function initMap(){
    var options = {
        zoom : 14,
        center:{lat:40.39801640241709,lng:49.87173725930695},
        mapTypeId: google.maps.MapTypeId.ROADMAP,
    }
    var azfilm = {lat:40.39801640241709,lng:49.87173725930695}
    var map  = new google.maps.Map(document.getElementById("map"),options);
    var marker = new google.maps.Marker({
            position : azfilm,
            map: map,
            animation: google.maps.Animation.DROP,
            icon : 'img/location.png',
      });
}
