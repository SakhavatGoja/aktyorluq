const single = document.querySelectorAll('.landing_single')

single && single.forEach(item =>{
  const clip = item.querySelector('.clip');
  clip && item.addEventListener('mouseenter',function(){
    item.classList.add('play')
    clip.play();
  })
  clip && item.addEventListener('mouseleave',function(){
    item.classList.remove('play')
    clip.pause();
  })

})

lightGallery(document.getElementById('aboutGalery'),{
  selector: 'a'
});

$('.hamburger_menu').on('click',function(){
  $('.right_side_menu').addClass('motion');
  $(this).addClass('quit')
})

$('.right_menu_top .menu_close').on('click',function(){
  $('.right_side_menu').removeClass('motion');
  $('.hamburger_menu').removeClass('quit')
})

$('.arrow_mobile').on('click',function(){
  $(this).parent().toggleClass('change');
  $('.center_education_sublinks').toggle()
})

let btn_active = parseInt($('.btn_active').attr('id'));
console.log($(`.person_single[data-id='${btn_active}']`))
$(`.person_single[data-id='${btn_active}']`).addClass('show')

$('.btn_education').on('click',function(){
  $('.btn_education').removeClass('btn_active');
  $(this).addClass('btn_active');
  let id = parseInt($(this).attr('id'));
  let single = $('.person_single');
  $('.person_single').removeClass('show')
  for (let i = 0; i < single.length; i++) {
    let dataId = parseInt($(single[i]).data('id'))
    if(id == dataId){
      $(single[i]).addClass('show');
    }
    else{
      null
    }
  }
})


$('body').on('click', function (e) {
  if ($('.right_side_menu').length !== 0
      && !$(e.target).hasClass('.right_side_menu')       
      && !$(e.target).parents('.right_side_menu').length 
      && !$(e.target).hasClass('hamburger_menu')
      && !$(e.target).parents('.hamburger_menu').length
      ){
     $('.right_side_menu').removeClass('motion')
     $('.hamburger_menu').removeClass('quit')
  }
});
