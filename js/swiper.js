var companySwiper = new Swiper(".infinity", {
  slidesPerView:'auto',
  spaceBetween: 50,
  loopAdditionalSlides: 20,
  speed: 5000,
  loop: true,
  allowTouchMove: false,
  autoplay: {
      delay: 0,
      disableOnInteraction: false
  },
});

var companySwipe2r = new Swiper(".infinity_one", {
  slidesPerView:'auto',
  spaceBetween: 50,
  loopAdditionalSlides: 20,
  speed: 5000,
  loop: true,
  allowTouchMove: false,
  autoplay: {
      delay: 0,
      disableOnInteraction: false
  },
});
