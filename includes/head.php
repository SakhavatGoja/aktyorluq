<html>
  <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0" >
      <title>Aktyorluq</title>
      <link rel="stylesheet" href="css/bootstrap.css">
      <link rel="stylesheet" href="css/bootstrap-grid.css">
      <link rel="stylesheet" href="css/default.css">
      <link rel="stylesheet" href="css/aos.min.css">
      <link rel="stylesheet" href="css/swiper.css">
      <link rel="stylesheet" href="css/swiper-bullet.css">
      <link rel="stylesheet" href="css/lightgalery.min.css">
      <link rel="stylesheet" href="css/common.css">
  </head>
  <body>
