<header>

  <div class="container">
    <div class="row">
      <div class="header_main_container">
        <a href="index.php" class="logo">
          <img src="img/logo.png" alt="" class="black-logo">
          <img src="img/logo-white.png" alt="" class="white-logo">
        </a>
        <button class="hamburger_menu">
          <img src="img/hamburger.svg" alt="">
        </button>
      </div>
    </div>
  </div>
  
  <div class="right_side_menu" style="transform: translateX(370px)">
    <div class="lent_side"></div>
    
    <div class="right_menu_blank">
      <div class="links">
        <a href="#">Haqqımızda</a>
        <a href="#">Komandamız</a>
        <a href="#">Layihələr</a>
        <div class="edu_link">
          <div class="center_education_sublinks">
            <div class="center_ul">
              <a href="#">Aktyorluq sənəti</a>
              <a href="#">Vokal</a>
              <a href="#">Qabiliyyət imtahanına hazırlıq</a>
              <a href="#">Aktyorluq sənəti</a>
              <a href="#">Vokal</a>
              <a href="#">Qabiliyyət imtahanına hazırlıq</a>
              <a href="#">Aktyorluq sənəti</a>
            </div>
          </div>
          <div class="edu_link_compact">
            <button>
              <img src="img/arrow_mobile.svg" alt="" class="arrow_mobile">
              <img src="img/arrow_link.svg" alt="">
            </button>
            <a href="#"> Tədris</a>
          </div>
        </div>
        <a href="#">Məzunlarımız</a>
        <a href="#">Əlaqə</a>

        <div class="right_menu_top">
          <div class="languages">
            <a href="#">En</a>
            <a href="#">Ru</a>
          </div>
          <button class="menu_close">
            <img src="img/esc.svg" alt="">
          </button>
        </div>

      </div>

    </div>
  </div>

</header>

