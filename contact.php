<?php
    include("includes/head.php");
?>


<section class="contact">
    <?php
        include("includes/header.php");
    ?>
    <div class="container">
      <div class="row">
        <div class="contact_inner_container w-100">
          <div class="breadcrumbs">
            <a href="index.php">Aktyorluq məktəbİ</a>
            <svg width="6" height="8" viewBox="0 0 6 8" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M5.47754 3.75398L1.0752 0.315506C1.06369 0.306449 1.04987 0.300821 1.03531 0.299268C1.02075 0.297714 1.00605 0.300298 0.992895 0.306723C0.97974 0.313148 0.968663 0.323153 0.960937 0.335589C0.953212 0.348026 0.94915 0.362389 0.94922 0.377029V1.13191C0.94922 1.17976 0.971681 1.22566 1.00879 1.25496L4.52441 4.00008L1.00879 6.7452C0.970704 6.77449 0.94922 6.82039 0.94922 6.86824V7.62313C0.94922 7.68856 1.02441 7.72469 1.0752 7.68465L5.47754 4.24617C5.51496 4.21698 5.54523 4.17965 5.56604 4.13701C5.58686 4.09436 5.59768 4.04753 5.59768 4.00008C5.59768 3.95262 5.58686 3.90579 5.56604 3.86315C5.54523 3.82051 5.51496 3.78317 5.47754 3.75398Z" fill="black"/>
            </svg>
            <span>Əlaqə</span>
          </div>

          <div class="contact_grid_box">
            <div class="contact_left">
              <p class="orange_title">Əlaqə</p>
              <div class="contact_desc">
                <div class="contact_row">
                  <svg width="21" height="21" viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M20.7962 14.3548L14.7808 13.6585L11.9043 16.5349C8.67405 14.8912 6.02589 12.2545 4.38221 9.01278L7.27007 6.12492L6.57379 0.132324H0.284413C-0.377626 11.7523 9.17629 21.3062 20.7962 20.6441V14.3548V14.3548Z" fill="#EC5A3B"/>
                  </svg>

                  <div class="phones">
                    <p>+99455 211 11 16</p>
                    <p>+99455 211 11 16</p>
                  </div>
                </div>
                <div class="contact_row">
                  <svg width="17" height="23" viewBox="0 0 17 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8.69716 0.0854492C4.27976 0.0854492 0.707031 3.65818 0.707031 8.07558C0.707031 14.0682 8.69716 22.9144 8.69716 22.9144C8.69716 22.9144 16.6873 14.0682 16.6873 8.07558C16.6873 3.65818 13.1146 0.0854492 8.69716 0.0854492ZM8.69716 10.9292C7.12197 10.9292 5.84354 9.65078 5.84354 8.07558C5.84354 6.50038 7.12197 5.22196 8.69716 5.22196C10.2724 5.22196 11.5508 6.50038 11.5508 8.07558C11.5508 9.65078 10.2724 10.9292 8.69716 10.9292Z" fill="#EC5A3B"/>
                  </svg>

                  <p>Bakı ş. Nərimanov ray. Ziya Bünyadov küç</p>
                </div>
                <div class="contact_row">
                  <svg width="25" height="19" viewBox="0 0 25 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M22.3513 0.115723H2.87063C1.53134 0.115723 0.447722 1.14303 0.447722 2.39862L0.435547 16.096C0.435547 17.3516 1.53134 18.3789 2.87063 18.3789H22.3513C23.6906 18.3789 24.7864 17.3516 24.7864 16.096V2.39862C24.7864 1.14303 23.6906 0.115723 22.3513 0.115723ZM22.3513 4.68151L12.611 10.3887L2.87063 4.68151V2.39862L12.611 8.10585L22.3513 2.39862V4.68151Z" fill="#EC5A3B"/>
                  </svg>

                  <p>info@azclimart.az</p>
                </div>
              </div>
            </div>
            <div class="contact_right">
              <div class="left_lent contact_lent"></div>
              <div id="map"></div>
              <div class="right_lent contact_lent"></div>
            </div>
          </div>

        </div>
      </div>
    </div>
    <?php
        include("includes/footer.php");
    ?>
</section>
<script  src="js/map.js"></script>
<script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA4SgjsTpN_D4nuu-O2Dn8f5aeK7dLjwoQ&callback=initMap&libraries=places" async defer></script>
<?php
    include("includes/script.php");
?>