<?php
    include("includes/head.php");
?>


<section class="index">
    <?php
        include("includes/header.php");
    ?>
    <div class="index_main_landing">
        <div class="index_parent_grid_container">
            <div class="landing_single" style="background-image:url('img/img1.png')">
            </div>
            <div class="landing_single" style="background-image:url('img/img2.png')">
                <video src="video/test.mp4" loop class="clip" muted></video>
            </div>
            <div class="landing_single" style="background-image:url('img/img2.png')">
                <video src="video/test.mp4" loop class="clip" muted></video>
            </div>
            <div class="landing_single" style="background-image:url('img/img2.png')">
                <video src="video/test.mp4" loop class="clip" muted></video>
            </div>
        </div>
        <div class="go_forward">
            <div class="container">
                <div class="row">
                    <p class="forward_title">İrəli çıx!</p>
                </div>
            </div>
            <div class="swiper-container infinity_one">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <p>Səhnə sənindir</p>
                    </div>
                    <div class="swiper-slide">
                        <p>Səhnə sənindir</p>
                    </div>
                    <div class="swiper-slide">
                        <p>Səhnə sənindir</p>
                    </div>
                    <div class="swiper-slide">
                        <p>Səhnə sənindir</p>
                    </div>
                    <div class="swiper-slide">
                        <p>Səhnə sənindir</p>
                    </div>
                    <div class="swiper-slide">
                        <p>Səhnə sənindir</p>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="forward_bottom">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#formModal">
                        Müraciət et
                    </button>
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="index_who_are">
        <div class="container">
            <div class="row">
                <div class="i_who_grid">
                    <div class="who_left">
                        <p class="top_who_title">Haqımızda</p>
                        <p class="bottom_who_title">Biz kimik ?</p>
                    </div>
                    <p class="content">
                        <b>Azərbaycan Film Akademiyası</b>, Aktyorluq Məktəbində müasir aktyorluq metodları tədris edilir. 
                        Effekti aktyor olmağın incəlikləri və yaxşı aktyor olmaq üçün rejissor nəyə diqqət etməlidir.
                    </p>
                    <div class="who_right">
                        <div class="i_who_top">
                            <div class="lent_same left_lent"></div>
                            <div class="center_image">
                                <img src="img/test2.png" alt="">
                            </div>
                            <div class="lent_same right_lent"></div>
                        </div>
                        <a href="#" class="who_link">daha ətraflı<img src="img/arrow_orange.svg" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="index_education">
        <div class="container">
            <div class="row">
                <div class="index_inner_education">
                    <div class="i_edu_left">
                        <p class="little_inner">Tədris</p>
                        <div class="double_title">
                            <p>Kim olmaq</p>
                            <p>istəyirsən?</p>
                        </div>
                        <div class="index_left_box">
                            <a href="#" class="more">daha çox</a>
                            <div class="index_left_img">
                                <img src="img/left.png" alt="">
                            </div>
                            <button class="yellow_stroke">İndi müraciət et</button>
                        </div>
                    </div>
                    <div class="i_edu_right">
                        <div class="index_edu_row">
                            <a href="motivation_inner.php" class="mt_single" >
                                <div>
                                    <p class="mt_number">01</p>
                                </div>
                                <div class="mt_center">
                                    <p class="mt_title">Aktyorluq sənəti</p>
                                    <p class="mt_content">
                                        Aktyor olmaq üçün Aktyorluq kurslarına və ya incəsənət universitetində Aktyorluq kurslarına
                                    </p>
                                </div>
                                <div class="mt_arrow"><img src="img/mt_arrow.png" alt=""></div>
                            </a>
                        </div>
                        <div class="index_edu_row">
                            <a href="motivation_inner.php" class="mt_single" >
                                <div>
                                    <p class="mt_number">01</p>
                                </div>
                                <div class="mt_center">
                                    <p class="mt_title">Aktyorluq sənəti</p>
                                    <p class="mt_content">
                                        Aktyor olmaq üçün Aktyorluq kurslarına və ya incəsənət universitetində Aktyorluq kurslarına
                                    </p>
                                </div>
                                <div class="mt_arrow"><img src="img/mt_arrow.png" alt=""></div>
                            </a>
                        </div>
                        <div class="index_edu_row">
                            <a href="motivation_inner.php" class="mt_single" >
                                <div>
                                    <p class="mt_number">01</p>
                                </div>
                                <div class="mt_center">
                                    <p class="mt_title">Aktyorluq sənəti</p>
                                    <p class="mt_content">
                                        Aktyor olmaq üçün Aktyorluq kurslarına və ya incəsənət universitetində Aktyorluq kurslarına
                                    </p>
                                </div>
                                <div class="mt_arrow"><img src="img/mt_arrow.png" alt=""></div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="index_school">
        <div class="container">
            <div class="row">
                <div class="index_header w-100">
                    <p class="orange_title">Məktəbimiz</p>
                    <a href="#" class="who_link">daha ətraflı<img src="img/arrow_orange.svg" alt=""></a>
                </div>
            </div>
        </div>
        <div class="swiper-container infinity">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <p>Səhnə sənindir</p>
                </div>
                <div class="swiper-slide">
                    <p>Səhnə sənindir</p>
                </div>
                <div class="swiper-slide">
                    <p>Səhnə sənindir</p>
                </div>
                <div class="swiper-slide">
                    <p>Səhnə sənindir</p>
                </div>
                <div class="swiper-slide">
                    <p>Səhnə sənindir</p>
                </div>
                <div class="swiper-slide">
                    <p>Səhnə sənindir</p>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
              <div class="person_box">
                <div class="person_single">
                  <div class="person_img"><img src="img/person.png" alt=""></div>
                  <div class="person_info">
                    <p class="person_name">Namaz Bayramov</p>
                    <p class="person_job">Şagird</p>
                  </div>
                </div>
                <div class="person_single">
                  <div class="person_img"><img src="img/person2.png" alt=""></div>
                  <div class="person_info">
                    <p class="person_name">Namaz Bayramov</p>
                    <p class="person_job">Şagird</p>
                  </div>
                </div>
                <div class="person_single">
                  <div class="person_img"><img src="img/person3.png" alt=""></div>
                  <div class="person_info">
                    <p class="person_name">Namaz Bayramov</p>
                    <p class="person_job">Şagird</p>
                  </div>
                </div>
                <div class="person_single">
                  <div class="person_img"><img src="img/person4.png" alt=""></div>
                  <div class="person_info">
                    <p class="person_name">Namaz Bayramov</p>
                    <p class="person_job">Şagird</p>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>

    <div class="index_four_grid_section">
        <div class="container">
            <div class="row">
                <div class="index_films_grid">
                    <div class="index_film">
                        <div class="film_img"><img src="img/film_1.png" alt=""></div>
                        <div class="film_right_content">
                            <p class="film_name">Ördək sindromu</p>
                            <div class="film_bottom_info">
                                <span class="film_date">14.09.2017</span>
                                <p class="film_type">Kino, Poster, Yeni</p>
                            </div>
                        </div>
                    </div>
                    <div class="index_film">
                        <div class="film_img"><img src="img/film_1.png" alt=""></div>
                        <div class="film_right_content">
                            <p class="film_name">Ördək sindromu</p>
                            <div class="film_bottom_info">
                                <span class="film_date">14.09.2017</span>
                                <p class="film_type">Kino, Poster, Yeni</p>
                            </div>
                        </div>
                    </div>
                    <div class="index_film">
                        <div class="film_img"><img src="img/film_1.png" alt=""></div>
                        <div class="film_right_content">
                            <p class="film_name">Ördək sindromu</p>
                            <div class="film_bottom_info">
                                <span class="film_date">14.09.2017</span>
                                <p class="film_type">Kino, Poster, Yeni</p>
                            </div>
                        </div>
                    </div>
                    <div class="index_film">
                        <div class="film_img"><img src="img/film_1.png" alt=""></div>
                        <div class="film_right_content">
                            <p class="film_name">Ördək sindromu</p>
                            <div class="film_bottom_info">
                                <span class="film_date">14.09.2017</span>
                                <p class="film_type">Kino, Poster, Yeni</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="film_absolute_box">
            <p class="orange_title">Biz nə edirik?</p>
            <a href="#">daha çox</a>
        </div>
    </div>


    




    <div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="container">
                <div class="row">
                    <div class="modal-content">
                        <button type="button" class="close_form" data-dismiss="modal" aria-label="Close">
                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M15.5837 1.944L14.0562 0.416504L8.00033 6.47234L1.94449 0.416504L0.416992 1.944L6.47283 7.99984L0.416992 14.0557L1.94449 15.5832L8.00033 9.52734L14.0562 15.5832L15.5837 14.0557L9.52783 7.99984L15.5837 1.944Z" fill="white"/>
                            </svg>
                        </button>
                        <div class="modal_left">
                            <p class="title_left">Bizə müraciət edin</p>
                        </div>
                        <div class="modal_right">
                            <p class="title_right">Formu doldur</p>
                            <form action="">
                                <input type="text" placeholder="Ad , Soyad" required>
                                <input type="text" placeholder="Təhsiliniz" required>
                                <input type="text" placeholder="Fəaliyyətiniz" required>
                                <input type="number" placeholder="Əlaqə nömrəniz" required>
                                <input type="text" placeholder="Faktiki yaşayış ünvanınız" required>
                                <textarea placeholder="Nəyə görə aktyorluq?"></textarea>
                                <button type="submit" class="orange_btn">Müraciət et</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
        include("includes/footer.php");
    ?>
</section>

<?php
    include("includes/script.php");
?>
