<?php
    include("includes/head.php");
?>


<section class="about">
    <?php
        include("includes/header.php");
    ?>
    <div class="container">
      <div class="row">
        <div class="about_main_container w-100">
          <div class="breadcrumbs">
            <a href="index.php">Aktyorluq məktəbİ</a>
            <svg width="6" height="8" viewBox="0 0 6 8" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M5.47754 3.75398L1.0752 0.315506C1.06369 0.306449 1.04987 0.300821 1.03531 0.299268C1.02075 0.297714 1.00605 0.300298 0.992895 0.306723C0.97974 0.313148 0.968663 0.323153 0.960937 0.335589C0.953212 0.348026 0.94915 0.362389 0.94922 0.377029V1.13191C0.94922 1.17976 0.971681 1.22566 1.00879 1.25496L4.52441 4.00008L1.00879 6.7452C0.970704 6.77449 0.94922 6.82039 0.94922 6.86824V7.62313C0.94922 7.68856 1.02441 7.72469 1.0752 7.68465L5.47754 4.24617C5.51496 4.21698 5.54523 4.17965 5.56604 4.13701C5.58686 4.09436 5.59768 4.04753 5.59768 4.00008C5.59768 3.95262 5.58686 3.90579 5.56604 3.86315C5.54523 3.82051 5.51496 3.78317 5.47754 3.75398Z" fill="black"/>
            </svg>
            <span>Haqqımızda</span>
          </div>
          <div class="about_top">
            <div class="who_are_we">
              <div class="about_title">
                <p class="top_title">Haqımızda</p>
                <p class="main_title">Biz kimik ?</p>
              </div>
              <div class="who_img"><img src="img/who_are_we.png" alt=""></div>
            </div>
            <p class="text_center_about">
              Azərbaycan Film Akademiyası, Aktyorluq Məktəbində müasir aktyorluq metodları tədris edilir. Effekti aktyor olmağın incəlikləri və yaxşı
               aktyor olmaq üçün rejissor nəyə diqqət etməlidir və buna oxşar bütün sualların cavabı üçün Azərbaycan film akademiyasının aktyorluq 
               kurslarına qatıla bilərsiniz. Azərbaycan Film Akademiyası, Azərbaycan film sahəsinin inkişafı üçün yaradılmış Məhdud Məsuliyyətli Cəmiyyətdir
            </p>
            <div class="about_info_box">
              <div class="about_info_img">
                <img src="img/a_info.png" alt="">
              </div>
              <p>
                Film istehsalı və kadrların yetişməsi üçün kinomatqrafiyanın bütün sahələri üzrə tədrisini həyata keçirir. Qısa zamanda önəmli 
                inkişaf artımını hədəfləmişik. Qurum təhsil alan tələbələri sertifikatla təmin edir və onların qısa və tammetrajlı filmlərdə rol 
                almasını təşkil edir.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="company_center">
      <div class="lent lent_top"></div>
      <div class="our_company">
        <div class="container">
          <div class="row">
            <div class="company_main_box">
              <p class="company_title">Öz şirkətlərimiz</p>
              <div class="company_grid_box">
                <div class="company_single">
                  <div class="company_left">
                    <p class="company_date">04/01/20</p>
                    <div class="company_img"><img src="img/company.png" alt=""></div>
                  </div>
                  <div class="company_right">
                    <p class="right_title">FLIX</p>
                    <p class="right_content">
                      Film istehsalı və kadrların yetişməsi üçün kinomatqrafiyanın bütün sahələri
                      üzrə tədrisini həyata keçirir. Qısa zamanda önəmli inkişaf artımını hədəfləmişik.
                    </p>
                  </div>
                </div>
                <div class="company_single">
                  <div class="company_left">
                    <p class="company_date">04/01/20</p>
                    <div class="company_img"><img src="img/company.png" alt=""></div>
                  </div>
                  <div class="company_right">
                    <p class="right_title">FLIX</p>
                    <p class="right_content">
                      Film istehsalı və kadrların yetişməsi üçün kinomatqrafiyanın bütün sahələri
                      üzrə tədrisini həyata keçirir. Qısa zamanda önəmli inkişaf artımını hədəfləmişik.
                    </p>
                  </div>
                </div>
                <div class="company_single">
                  <div class="company_left">
                    <p class="company_date">04/01/20</p>
                    <div class="company_img"><img src="img/company.png" alt=""></div>
                  </div>
                  <div class="company_right">
                    <p class="right_title">FLIX</p>
                    <p class="right_content">
                      Film istehsalı və kadrların yetişməsi üçün kinomatqrafiyanın bütün sahələri
                      üzrə tədrisini həyata keçirir. Qısa zamanda önəmli inkişaf artımını hədəfləmişik.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
      </div>
      <div class="lent lent_bottom"></div>
    </div>
    <div class="galery_about">
      <div class="container">
        <div class="row">
          <p class="galery_title">Qalereya</p>
          <div class="galery_grid" id="aboutGalery">
            <div class="galery_row">
              <a href="img/galery1.png" class="grid-item">
                <img src="img/galery1.png" alt="">
              </a>
              <div class="right_side_galery">
                <a href="img/galery2.png" class="grid-item">
                  <img src="img/galery2.png" alt="">
                </a>
                <a href="img/galery2.png" class="grid-item">
                  <img src="img/galery2.png" alt="">
                </a>
              </div>
            </div>
            <div class="galery_row">
              <a href="img/galery1.png" class="grid-item">
                <img src="img/galery1.png" alt="">
              </a>
              <div class="right_side_galery">
                <a href="img/galery2.png" class="grid-item">
                  <img src="img/galery2.png" alt="">
                </a>
                <a href="img/galery2.png" class="grid-item">
                  <img src="img/galery2.png" alt="">
                </a>
              </div>
            </div>
            <div class="galery_row">
              <a href="img/galery1.png" class="grid-item">
                <img src="img/galery1.png" alt="">
              </a>
              <div class="right_side_galery">
                <a href="img/galery2.png" class="grid-item">
                  <img src="img/galery2.png" alt="">
                </a>
                <a href="img/galery2.png" class="grid-item">
                  <img src="img/galery2.png" alt="">
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


    <?php
        include("includes/footer.php");
    ?>
</section>

<?php
    include("includes/script.php");
?>
