<?php
    include("includes/head.php");
?>


<section class="team">
    <?php
        include("includes/header.php");
    ?>
    <div class="container">
      <div class="row">
        <div class="team_main_container">
          <div class="breadcrumbs">
            <a href="index.php">Aktyorluq məktəbİ</a>
            <svg width="6" height="8" viewBox="0 0 6 8" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M5.47754 3.75398L1.0752 0.315506C1.06369 0.306449 1.04987 0.300821 1.03531 0.299268C1.02075 0.297714 1.00605 0.300298 0.992895 0.306723C0.97974 0.313148 0.968663 0.323153 0.960937 0.335589C0.953212 0.348026 0.94915 0.362389 0.94922 0.377029V1.13191C0.94922 1.17976 0.971681 1.22566 1.00879 1.25496L4.52441 4.00008L1.00879 6.7452C0.970704 6.77449 0.94922 6.82039 0.94922 6.86824V7.62313C0.94922 7.68856 1.02441 7.72469 1.0752 7.68465L5.47754 4.24617C5.51496 4.21698 5.54523 4.17965 5.56604 4.13701C5.58686 4.09436 5.59768 4.04753 5.59768 4.00008C5.59768 3.95262 5.58686 3.90579 5.56604 3.86315C5.54523 3.82051 5.51496 3.78317 5.47754 3.75398Z" fill="black"/>
            </svg>
            <span>Komandamız</span>
          </div>
          <div class="section_title_box">
            <p class="orange_title">Komandamız</p>
            <p class="right_content">
              Film istehsalı və kadrların yetişməsi üçün kinomatqrafiyanın bütün sahələri üzrə tədrisini
               həyata keçirir. Qısa zamanda önəmli inkişaf artımını hədəfləmişik.
            </p>
          </div>
          <div class="team_main_grid">
            <div class="team_one_fraction" >
              <div class="team_one_img"><img src="img/person2.png" alt=""></div>
              <div class="team_right_content">
                <p class="t_name">Aygün Mehdizadə</p>
                <p class="t_job">Aktrisa</p>
                <p class="t_info">
                  Film istehsalı və kadrların yetişməsi üçün kinomatqrafiyanın bütün sahələri üzrə tədrisini həyata keçirir. Qısa zamanda 
                  önəmli inkişaf artımını hədəfləmişik. Film istehsalı və kadrların yetişməsi üçün kinomatqrafiyanın bütün sahələri üzrə 
                  tədrisini həyata keçirir. Qısa zamanda önəmli inkişaf artımını hədəfləmişik. Film istehsalı və kadrların yetişməsi üçün 
                  kinomatqrafiyanın bütün sahələri üzrə tədrisini həyata keçirir. Qısa zamanda önəmli inkişaf artımını hədəfləmişik.
                  Qısa zamanda önəmli inkişaf artımını hədəfləmişik. Film istehsalı və kadrların yetişməsi üçün kinomatqrafiyanın bütün 
                  sahələri üzrə tədrisini həyata keçirir. Qısa zamanda önəmli inkişaf artımını hədəfləmişik.
                 Qısa zamanda önəmli inkişaf artımını hədəfləmişik. Film istehsalı və kadrların yetişməsi üçün kinomatqrafiyanın bütün 
                  sahələri üzrə tədrisini həyata keçirir. Qısa zamanda önəmli inkişaf artımını hədəfləmişik.Qısa zamanda önəmli inkişaf artımını hədəfləmişik. Film istehsalı və kadrların yetişməsi üçün kinomatqrafiyanın bütün 
                  sahələri üzrə tədrisini həyata keçirir. Qısa zamanda önəmli inkişaf artımını hədəfləmişik.Qısa zamanda önəmli inkişaf artımını hədəfləmişik. Film istehsalı və kadrların yetişməsi üçün kinomatqrafiyanın bütün 
                  sahələri üzrə tədrisini həyata keçirir. Qısa zamanda önəmli inkişaf artımını hədəfləmişik.
                </p>
              </div>
            </div>
            <div class="team_one_fraction" >
              <div class="team_one_img"><img src="img/person2.png" alt=""></div>
              <div class="team_right_content">
                <p class="t_name">Aygün Mehdizadə</p>
                <p class="t_job">Aktrisa</p>
                <p class="t_info">
                  Film istehsalı və kadrların yetişməsi üçün kinomatqrafiyanın bütün sahələri üzrə tədrisini həyata keçirir. Qısa zamanda 
                  önəmli inkişaf artımını hədəfləmişik. Film istehsalı və kadrların yetişməsi üçün kinomatqrafiyanın bütün sahələri üzrə 
                  tədrisini həyata keçirir. Qısa zamanda önəmli inkişaf artımını hədəfləmişik. Film istehsalı və kadrların yetişməsi üçün 
                  kinomatqrafiyanın bütün sahələri üzrə tədrisini həyata keçirir. Qısa zamanda önəmli inkişaf artımını hədəfləmişik.
                  Qısa zamanda önəmli inkişaf artımını hədəfləmişik. Film istehsalı və kadrların yetişməsi üçün kinomatqrafiyanın bütün 
                  sahələri üzrə tədrisini həyata keçirir. Qısa zamanda önəmli inkişaf artımını hədəfləmişik.
                 Qısa zamanda önəmli inkişaf artımını hədəfləmişik. Film istehsalı və kadrların yetişməsi üçün kinomatqrafiyanın bütün 
                  sahələri üzrə tədrisini həyata keçirir. Qısa zamanda önəmli inkişaf artımını hədəfləmişik.Qısa zamanda önəmli inkişaf artımını hədəfləmişik. Film istehsalı və kadrların yetişməsi üçün kinomatqrafiyanın bütün 
                  sahələri üzrə tədrisini həyata keçirir. Qısa zamanda önəmli inkişaf artımını hədəfləmişik.Qısa zamanda önəmli inkişaf artımını hədəfləmişik. Film istehsalı və kadrların yetişməsi üçün kinomatqrafiyanın bütün 
                  sahələri üzrə tədrisini həyata keçirir. Qısa zamanda önəmli inkişaf artımını hədəfləmişik.
                </p>
              </div>
            </div>
            <div class="team_one_fraction" >
              <div class="team_one_img"><img src="img/person2.png" alt=""></div>
              <div class="team_right_content">
                <p class="t_name">Aygün Mehdizadə</p>
                <p class="t_job">Aktrisa</p>
                <p class="t_info">
                  Film istehsalı və kadrların yetişməsi üçün kinomatqrafiyanın bütün sahələri üzrə tədrisini həyata keçirir. Qısa zamanda 
                  önəmli inkişaf artımını hədəfləmişik. Film istehsalı və kadrların yetişməsi üçün kinomatqrafiyanın bütün sahələri üzrə 
                  tədrisini həyata keçirir. Qısa zamanda önəmli inkişaf artımını hədəfləmişik. Film istehsalı və kadrların yetişməsi üçün 
                  kinomatqrafiyanın bütün sahələri üzrə tədrisini həyata keçirir. Qısa zamanda önəmli inkişaf artımını hədəfləmişik.
                  Qısa zamanda önəmli inkişaf artımını hədəfləmişik. Film istehsalı və kadrların yetişməsi üçün kinomatqrafiyanın bütün 
                  sahələri üzrə tədrisini həyata keçirir. Qısa zamanda önəmli inkişaf artımını hədəfləmişik.
                  Qısa zamanda önəmli inkişaf artımını hədəfləmişik. Film istehsalı və kadrların yetişməsi üçün kinomatqrafiyanın bütün 
                  sahələri üzrə tədrisini həyata keçirir. Qısa zamanda önəmli inkişaf artımını hədəfləmişik.Qısa zamanda önəmli inkişaf artımını hədəfləmişik. Film istehsalı və kadrların yetişməsi üçün kinomatqrafiyanın bütün 
                  sahələri üzrə tədrisini həyata keçirir. Qısa zamanda önəmli inkişaf artımını hədəfləmişik.Qısa zamanda önəmli inkişaf artımını hədəfləmişik. Film istehsalı və kadrların yetişməsi üçün kinomatqrafiyanın bütün 
                  sahələri üzrə tədrisini həyata keçirir. Qısa zamanda önəmli inkişaf artımını hədəfləmişik.
                </p>
              </div>
            </div>
            <div class="team_one_fraction" >
              <div class="team_one_img"><img src="img/person2.png" alt=""></div>
              <div class="team_right_content">
                <p class="t_name">Aygün Mehdizadə</p>
                <p class="t_job">Aktrisa</p>
                <p class="t_info">
                  Film istehsalı və kadrların yetişməsi üçün kinomatqrafiyanın bütün sahələri üzrə tədrisini həyata keçirir. Qısa zamanda 
                  önəmli inkişaf artımını hədəfləmişik. Film istehsalı və kadrların yetişməsi üçün kinomatqrafiyanın bütün sahələri üzrə 
                  tədrisini həyata keçirir. Qısa zamanda önəmli inkişaf artımını hədəfləmişik. Film istehsalı və kadrların yetişməsi üçün 
                  kinomatqrafiyanın bütün sahələri üzrə tədrisini həyata keçirir. Qısa zamanda önəmli inkişaf artımını hədəfləmişik.
                  Qısa zamanda önəmli inkişaf artımını hədəfləmişik. Film istehsalı və kadrların yetişməsi üçün kinomatqrafiyanın bütün 
                  sahələri üzrə tədrisini həyata keçirir. Qısa zamanda önəmli inkişaf artımını hədəfləmişik.
                  Qısa zamanda önəmli inkişaf artımını hədəfləmişik. Film istehsalı və kadrların yetişməsi üçün kinomatqrafiyanın bütün 
                  sahələri üzrə tədrisini həyata keçirir. Qısa zamanda önəmli inkişaf artımını hədəfləmişik.Qısa zamanda önəmli inkişaf artımını hədəfləmişik. Film istehsalı və kadrların yetişməsi üçün kinomatqrafiyanın bütün 
                  sahələri üzrə tədrisini həyata keçirir. Qısa zamanda önəmli inkişaf artımını hədəfləmişik.Qısa zamanda önəmli inkişaf artımını hədəfləmişik. Film istehsalı və kadrların yetişməsi üçün kinomatqrafiyanın bütün 
                  sahələri üzrə tədrisini həyata keçirir. Qısa zamanda önəmli inkişaf artımını hədəfləmişik.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


    <div class="lent"></div>
    <?php
        include("includes/footer.php");
    ?>
</section>

<?php
    include("includes/script.php");
?>
